-- SUMMARY --

The LT4L module creates a block within your Drupal installation that
allows users with a LibraryThing for Libraries account to connect the tagging of Drupal content and items in the library catalog.  This block is intended for use on a single node page. The module gathers all terms used for the node and then queries LT4L. The query includes the library's LT4L id, which is entered in this modules settings page. The module first attempts to retrieve the desired number which are tagged with all of the tags in use by this node. If enough results are not achieved, the list is completed with a random assortment of items tagged with only one of the node's terms.


-- REQUIREMENTS --

There are no requirements.


-- INSTALLATION --

Install like any other contributed module.


-- CUSTOMIZATION --

* Customize the LT4L block in Administer >> Site configuration >>
  LT4L settings.
  
  - LT4L id for your library
    
    The LibraryThing for Libraries ID number to use.
    
  - Title displayed by block
    
    The title which should be used to label this block. Use <none> to display no title.
    
  - Maximum number of items to display
    
    The maximum number of matching items from catalog to display in the block.  
    
  - Maximum number of requests to make to LT4L servers
  
    The maximum number of times this module will make an http request to the LT4L servers.
    
  - Vocabulary to use
  
    The id of the vocabulary which should be used to match LT4L tags. If this is left blank, all terms for a given node will be used.

  - HTML tag for items

    The tag used to wrap each catalog link found.

* Enable the block to appear on a single node page.  
  
  - Nodes must be tagged with taxonomy terms in order for any content to appear in this block.
  
  - Tags must match those in use by LT4L in order for matching catalog items to be found.
      
-- TROUBLESHOOTING --

* If the block does not display but is enabled, check to make sure that a valid LT4L id is entered in the LT4L settings page.

* If the block does not display but is enabled, make sure that the block is being used on a node

* If the wrong vocabulary is being used, check that you are using the correct vocabulary id on Administer >> Content >> Taxonomy.

-- CONTACT --

Current maintainers:
* Lisa Sawin (somanyfish) - http://drupal.org/user/646372

This project has been sponsored by:
* Winterson Design
  Visit http://www.wintersondesign.com for more information.

* The Westport Public Library
  Visit http://www.westportlibrary.org for more information.
