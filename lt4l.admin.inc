<?php
/**
 * @file
 * LT4L module admin functions
 */

/**
 * LT4L admin menu settings.
 */
function lt4l_admin() {
  $form = array();

  $form['lt4l_id'] = array(
    '#type' => 'textfield',
    '#title' => t('LT4L id for your library'),
    '#default_value' => variable_get('lt4l_id', NULL),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('The LibraryThing for Libraries ID number to use.'),
    '#required' => TRUE,
  );
  $form['lt4l_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Title displayed by block'),
    '#default_value' => variable_get('lt4l_subject', 'Related items'),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('The title which should be used to label this block.  Use <em>&lt;none&gt;</em> to display no title.'),
    '#required' => TRUE,
  );
  $form['lt4l_maxdisp'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of items to display'),
    '#default_value' => variable_get('lt4l_maxdisp', 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The maximum number of items to display in the block.'),
    '#required' => TRUE,
  );
  $form['lt4l_maxrequests'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of requests to make to LT4L servers'),
    '#default_value' => variable_get('lt4l_maxrequests', 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The maximum number of times this module will make an http request to the LT4L servers.'),
    '#required' => TRUE,
  );
  $form['lt4l_vocabulary'] = array(
    '#type' => 'textfield',
    '#title' => t('Vocabulary to use'),
    '#default_value' => variable_get('lt4l_vocabulary', NULL),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('The id of the vocabulary which should be used to match LT4L tags.  If this is left blank, all terms for a given node will be used.'),
    '#required' => FALSE,
  );
  $form['lt4l_item_tag'] = array(
    '#type' => 'select',
    '#title' => t('HTML tag for items'),
    '#default_value' => variable_get('lt4l_item_tag', 'li'),
    '#options' => array('li', 'ol', 'span', 'div', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
    '#description' => t('The tag used to wrap each catalog link found.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
